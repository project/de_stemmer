
Readme
------

This module implements a German stemming algorithm to improve German-language
searching with the Drupal built-in search.module.


It reduces each word in the index to its basic root or stem so that variations
on a word (e.g. "Menschlichkeit -> menschlich" or "Städte -> Stadt") are
considered equivalent when searching. This generally results in more
relevant results.


The module contains two (static) lists
* one list of stop words
* one (currently very small) list of exceptions

Please feel free to comment these lists ...


Currently stemming results in finding additional documents.
And the list of search results contains these documents as well.
But the matching words are not recognized and not marked by the search module 
what makes searching not realy easy.
To solve this a patch for the search module is included in this package. Applying this
patch should not harm the search module in any way.
